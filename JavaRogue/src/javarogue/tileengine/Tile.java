package javarogue.tileengine;

import javarogue.config.ConfigGraphics;
import javarogue.utility.Position;

/**
 * <h1>Tile</h1>
 * 
 * The collection of possible tiles to be "cut" out of the tileset. The
 * implementation is image-independent as long as the new tileset image respects
 * the same grid order of tile types. Use the getOrigin() function to get the
 * upper-right corner in the image. {@link ConfigView} provides the tileSize.
 * 
 *
 */
public enum Tile {

	VOID(0, 0),
	CORNER_NW(0, 1),
	WALL_N(0, 2),
	CORNER_NE(0, 3),
	BLOCK(0, 4),
	FLOOR_VAULT(0, 5),
	STAIRS_UP(1, 0),
	WALL_W(1, 1),
	FLOOR(1, 2),
	WALL_E(1, 3),
	DOOR_VERT(1, 4),
	DOOR_VERT_LOCKED(1, 5),
	STAIRS_DOWN(2, 0),
	CORNER_SW(2, 1),
	WALL_S(2, 2),
	CORNER_SE(2, 3),
	DOOR_HORZ(2, 4),
	DOOR_HORZ_LOCKED(2, 5),
	ALPHA(3, 0),
	WATER(3, 1),
	TRAP(3, 2),
	CHEST(3, 3),
	FOUNTAIN(3, 4),
	SHRINE(3, 5),
	WEAPON(4, 0),
	POTION(4, 1),
	ARMOR(4, 2),
	SCROLL(4, 3),
	OBJECT(4, 4),
	PLAYER(4, 5),
	SLIME(5, 0),
	RAT(5, 1),
	THIEF(5, 2),
	GOBLIN(5, 3),
	GOLEM(5, 4),
	BOSS(5, 5);

	private int tilesetRow;
	private int tilesetColumn;
	
	private Tile(int tilesetRow, int tilesetColumn) {
		this.tilesetRow = tilesetRow;
		this.tilesetColumn = tilesetColumn;
	}
	
	/**
	 * 
	 * @return The origin (upper-right corner) of the tile relative to the tileset.
	 *         It respects the tile size provided in the ViewConfig.
	 */
	public Position getOrigin() {
		return new Position(this.tilesetColumn * ConfigGraphics.tileSize(), this.tilesetRow * ConfigGraphics.tileSize());
	}

}
