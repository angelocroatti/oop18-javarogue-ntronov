package javarogue.generation;

import javarogue.level.Level;

/**
 * 
 * <h1>LevelFactory</h1>
 * 
 * Factory for game levels.
 * @see Level
 *
 */
public interface LevelFactory {


	/**
	 * Generate a Simple level based on depth.
	 * @param depth an integer between 0 and 2.
	 * @return a generated SimpleLevel
	 */
	public Level generateSimpleLevel(int depth);
	
	/**
	 * Generate a Boss level.
	 * @return a generated BossLevel
	 */
	public Level generateBossLevel();

}
