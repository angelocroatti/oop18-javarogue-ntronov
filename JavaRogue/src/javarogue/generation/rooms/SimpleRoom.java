package javarogue.generation.rooms;

import java.util.Random;

import javarogue.tileengine.Tile;
import javarogue.utility.Matrix;
import javarogue.utility.Position;

/**
 * <h1>SimpleRoom</h1>
 * 
 * A simple rectangle room.
 * @see Room
 *
 */
public class SimpleRoom extends AbstractRoom {

	/**
	 * Builds a new rectangle room.
	 * @param origin The top-right corner of the room.
	 * @param map The referenced tile map.
	 * @param generator The seeded random number generator.
	 */
	public SimpleRoom(Position origin, Matrix<Tile> map, Random generator) {
		super(origin, map, generator);
	}
	
	@Override
	public void generate() {
		// Fill with floor
		this.makeFloor();
		// Make corners
		this.makeCorners();
		// Make straight walls
		this.makeWalls();
	}
	
	private void makeFloor() {
		for (int i = 0; i < this.getHeight(); i++) {
			for (int j = 0; j < this.getWidth(); j++) {
				Position pos = new Position(this.getOrigin().getX() + i, this.getOrigin().getY() + j);
				this.getTileCoordinates().put(pos, Tile.FLOOR);
			}
		}
	}
	
	private void makeCorners() {
		Position northWest = new Position(this.getOrigin().getX(), this.getOrigin().getY());
		this.getTileCoordinates().put(northWest, Tile.CORNER_NW);
		Position northEast = new Position(this.getOrigin().getX(), this.getOrigin().getY() + this.getWidth() - 1);
		this.getTileCoordinates().put(northEast, Tile.CORNER_NE);
		Position southWest = new Position(this.getOrigin().getX() + this.getHeight() - 1, this.getOrigin().getY());
		this.getTileCoordinates().put(southWest, Tile.CORNER_SW);
		Position southEast = new Position(this.getOrigin().getX() + this.getHeight() - 1, this.getOrigin().getY() + this.getWidth() - 1);
		this.getTileCoordinates().put(southEast, Tile.CORNER_SE);
	}
	
	private void makeWalls() {
		for (int i = 1; i < this.getWidth() - 1; i++) {
			Position posN = new Position(this.getOrigin().getX(), this.getOrigin().getY() + i);
			Position posS = new Position(this.getOrigin().getX() + this.getHeight() - 1, this.getOrigin().getY() + i);
			this.getTileCoordinates().put(posN, Tile.WALL_N);
			this.getTileCoordinates().put(posS, Tile.WALL_S);
		}
		for (int i = 1; i < this.getHeight() - 1; i++) {
			Position posW = new Position(this.getOrigin().getX() + i, this.getOrigin().getY());
			Position posE = new Position(this.getOrigin().getX() + i, this.getOrigin().getY() + this.getWidth() - 1);
			this.getTileCoordinates().put(posW, Tile.WALL_W);
			this.getTileCoordinates().put(posE, Tile.WALL_E);
		}
	}

}
