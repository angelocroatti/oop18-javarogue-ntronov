package javarogue.generation.algorithms;

import java.util.Optional;

import javarogue.level.Level;

public class GenerationContext {

	private Optional<GenerationAlgorithm> generator;
		
	public void setGenerationAlgorithm(GenerationAlgorithm generator) {
		this.generator = Optional.of(generator);
	}
	
	public Level generateLevel() {
		if(this.generator.isPresent()) {
			return this.generator.get().generateLevel();
		} else {
			throw new IllegalStateException("Calling GenerationContext with no GenerationAlgorithm.");
		}
	}
	
}
