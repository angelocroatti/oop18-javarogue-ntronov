package javarogue.generation.algorithms;

import javarogue.level.Level;
import javarogue.level.LevelImpl;
import javarogue.player.Player;
import javarogue.tileengine.Tile;
import javarogue.utility.Matrix;
import javarogue.utility.Position;

/**
 * <h1>BossGeneration</h1>
 * 
 *	Generates a Boss type of level.
 */
public class BossGeneration implements GenerationAlgorithm {

	@Override
	public Level generateLevel() {
		Level level = new LevelImpl();
		
		level.setTileMap(new Matrix<>(15, 15));
		level.setObjectMap(new Matrix<>(15, 15));

		level.getTileMap().fill(Tile.BLOCK);
		level.getObjectMap().fill(Tile.ALPHA);

		level.getTileMap().doubleFor(1, level.getTileMap().getRows() - 1, 1, 1, level.getTileMap().getCols() - 1, 1, (i, j) -> {
			level.getTileMap().set(i, j, Tile.FLOOR);
		});
		
		level.getObjectMap().set(5, 5, Tile.STAIRS_UP);
		level.setPlayer(new Player(new Position(5,5)));
		level.getObjectMap().set(8, 8, Tile.BOSS);
		
		return level;
	}

	
}
