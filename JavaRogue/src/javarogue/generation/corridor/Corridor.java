package javarogue.generation.corridor;

/**
 * <h1>Corridor</h1>
 * 
 * A corridor is a game generation object that represents a path connecting two
 * rooms.
 *
 */
public interface Corridor {

	/**
	 * Generates the corridor on a tile map.
	 * 
	 */
	public void generate();

}
