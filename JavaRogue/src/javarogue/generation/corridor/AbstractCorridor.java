package javarogue.generation.corridor;

import java.util.List;

import javarogue.tileengine.Tile;
import javarogue.utility.Matrix;
import javarogue.utility.Position;

/**
 * <h1>AbstractCorridor</h1>
 * 
 * An implementation of {@link Corridor}.
 *
 */
public abstract class AbstractCorridor implements Corridor {

	private Position origin;
	private Position destination;
	private List<Position> path;

	/**
	 * Builds a corridor
	 * 
	 * @param origin Origin of the corridor.
	 * @param destination Destination if the corridor.
	 * @param map Tile map in which to generate the corridor
	 */
	public AbstractCorridor(Position origin, Position destination, Matrix<Tile> map) {
		this.map = map;
		if (!this.isInBounds(origin)) {
			throw new IllegalArgumentException("Supplied origin is invalid!");
		} else if (!this.isInBounds(destination)) {
			throw new IllegalArgumentException("Supplied destination is invalid!");
		}
		this.origin = origin;
		this.destination = destination;
	}
	
	private Matrix<Tile> map;
	public Matrix<Tile> getMap() {
		return map;
	}

	public void setMap(Matrix<Tile> map) {
		this.map = map;
	}

	public Position getOrigin() {
		return origin;
	}

	public void setOrigin(Position origin) {
		this.origin = origin;
	}

	public Position getDestination() {
		return destination;
	}

	public void setDestination(Position destination) {
		this.destination = destination;
	}

	public List<Position> getPath() {
		return path;
	}

	public void setPath(List<Position> path) {
		this.path = path;
	}

	/**
	 * 
	 * @param path adds tiles to the map from the path
	 */
	public void addTiles(List<Position> path) {
		// Simply iterate the path list and add floor tiles to the map accordingly
		for (Position pos : path) {
			this.map.set(pos.getX(), pos.getY(), Tile.FLOOR);
		}
	}

	private boolean isInBounds(Position pos) {
		return pos.getX() >= 0 && pos.getY() >= 0 && pos.getX() < this.map.getRows() && pos.getY() < this.map.getCols();
	}
}
