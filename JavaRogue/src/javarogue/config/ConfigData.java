package javarogue.config;

import java.util.Optional;

/**
 * <h1>ConfigData</h1>
 * 
 * A collection of static values used for project-wise data initialized at the
 * moment of building. Pre-build values are set to null.
 *
 */
public class ConfigData {

	private static Optional<Long> seed = Optional.empty();

	/**
	 * 
	 * @param seed seed for all the random generation
	 */
	public ConfigData(long seed) {
		ConfigData.seed = Optional.of(seed);
	}

	/**
	 * 
	 * @return seed to be used for random number generation
	 */
	public static Long seed() {
		if (seed.isPresent()) {
			return seed.get();
		} else {
			throw new IllegalStateException();
		}
	}

}
