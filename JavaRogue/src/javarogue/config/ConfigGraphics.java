package javarogue.config;

import java.util.Optional;

import javarogue.utility.Resolution;

/**
 * <h1>ConfigGraphics</h1>
 * 
 * A collection of static values used for project-wise
 * data initialized at the moment of building. Pre-build values are set
 * to null.
 *
 */
public class ConfigGraphics {

	private static Optional<Integer> resolutionWidth = Optional.empty();

	private static Optional<Integer> resolutionHeight = Optional.empty();

	private static Optional<Integer> tileSize = Optional.empty();

	private static Optional<Integer> qualityScale = Optional.empty();

	private static Optional<String> tilesetName = Optional.empty();

	private static Optional<Boolean> fullscreen = Optional.empty();

	/**
	 * Set config's values. Must be called prior to launching the game window.
	 * 
	 * @param resolution Game window resolution
	 * @param fullscreen Whether fullscreen mode is enabled
	 * @param tileSize size of a tile in the tileset
	 * @param qualityScale "quality" image upscaling factor
	 * @param tilesetName
	 */
	public ConfigGraphics(Resolution resolution, boolean fullscreen, int tileSize, int qualityScale, String tilesetName) {
		ConfigGraphics.resolutionWidth = Optional.of(resolution.getWidth());
		ConfigGraphics.resolutionHeight = Optional.of(resolution.getHeight());
		ConfigGraphics.fullscreen = Optional.of(fullscreen);
		ConfigGraphics.tileSize = Optional.of(tileSize);
		ConfigGraphics.qualityScale = Optional.of(qualityScale);
		ConfigGraphics.tilesetName = Optional.of(tilesetName + ".png");
	}
	
	/**
	 * 
	 * @return The game window resolution width in pixels.
	 */
	public static Integer resolutionWidth() {
		if(resolutionWidth.isPresent()) {
			return resolutionWidth.get();
		} else {
			throw new IllegalStateException();
		}
	}

	/**
	 * 
	 * @return The game window resolution height in pixels.
	 */
	public static Integer resolutionHeight() {
		if(resolutionHeight.isPresent()) {
			return resolutionHeight.get();
		} else {
			throw new IllegalStateException();
		}
	}

	/**
	 * 
	 * @return The tileset's single tile size.
	 */
	public static Integer tileSize() {
		if(tileSize.isPresent()) {
			return tileSize.get();
		} else {
			throw new IllegalStateException();
		}
	}

	/**
	 * 
	 * @return The scale of the saved Tileset, impacts performance!
	 */
	public static Integer qualityScale() {
		if(qualityScale.isPresent()) {
			return qualityScale.get();
		} else {
			throw new IllegalStateException();
		}
	}

	/**
	 * 
	 * @return The path the tileset to be loaded.
	 */
	public static String tilesetPath() {
		if(tilesetName.isPresent()) {
			return tilesetName.get();
		} else {
			throw new IllegalStateException();
		}
	}

	/**
	 * 
	 * @return The boolean indicating whether the fullscreen mode is engaged or not.
	 */
	public static Boolean fullscreen() {
		if(fullscreen.isPresent()) {
			return fullscreen.get();
		} else {
			throw new IllegalStateException();
		}
	}

}
