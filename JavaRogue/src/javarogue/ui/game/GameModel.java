package javarogue.ui.game;

import java.util.Optional;

import javarogue.level.Level;

/**
 * <h1>GameModel</h1>
 * 
 * Model of the Game Window, handles the level data.
 *
 */
public interface GameModel {

	/**
	 * 
	 * Changes the current active level.
	 * 
	 * @param level Depth of the level to generate.
	 */
	public void changeLevel(int level);
	
	/**
	 * Generates a sequence of levels for the game.
	 */
	public void generateLevels();

	/**
	 * 
	 * @return Optional of the generated Level or Empty if none has been generated.
	 */
	public Optional<Level> getCurrentLevel();
	
	/**
	 * 
	 * @param controller Controller to be linked with this model
	 */
	public void setController(GameController controller);
	
	/**
	 * 
	 * @return current depth level
	 */
	public int getCurrentDepth();

}
