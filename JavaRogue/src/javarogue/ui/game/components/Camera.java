package javarogue.ui.game.components;

import javafx.scene.canvas.GraphicsContext;
import javarogue.config.ConfigGraphics;
import javarogue.level.Level;
import javarogue.tileengine.Tile;
import javarogue.tileengine.TileSet;
import javarogue.ui.game.GameViewImpl;
import javarogue.utility.Matrix;
import javarogue.utility.Position;
import javarogue.utility.Translation;

/**
 * <h1>Camera</h1>
 * 
 * A GUI component that handles game's "viewport".
 * @see GameViewImpl
 *
 */
public class Camera {

	private GraphicsContext context;
	private Level level;
	private Position origin;
	private TileSet tileset;
	
	private final int ROW_NUM = 11;
	private final int COL_NUM = 18;
	private double tileSize;
	
	/**
	 * 
	 * @param context Rendering context
	 */
	public Camera(GraphicsContext context) {
		this.context = context;
		// Load tileset
		this.tileset = new TileSet(ConfigGraphics.tilesetPath());
	}
	
	/**
	 * 
	 * @param level level to be linked with Camera
	 */
	public void setLevel(Level level) {
		this.level = level;
	}
	
	/**
	 * 
	 * @param origin top left corner of viewable area on the game map matrix
	 */
	public void setOrigin(Position origin) {
		this.origin = origin;
	}

	/**
	 * 
	 * @param trans translates the origin point of the camera 
	 */
	public void move(Translation trans) {
		this.origin.translate(trans.getDx(), trans.getDy());
	}

	/**
	 *  renders the camera data on the context.
	 */
	public void draw() {
		// Draw tile matrix based on camera origin and scale
		this.tileSize = this.context.getCanvas().getWidth() / COL_NUM;
		this.setOrigin(new Position(this.level.getPlayerPos().getX() - ROW_NUM / 2 + 1,
				this.level.getPlayerPos().getY() - COL_NUM / 2 + 1));
		this.drawMatrix(this.level.getTileMap());
		this.drawMatrix(this.level.getObjectMap());
	}

	private void drawMatrix(Matrix<Tile> matrix) {
		for (int i = 0; i < ROW_NUM; i++) {
			for (int j = 0; j < COL_NUM; j++) {
				Tile tile;
				if (this.origin.getX() + i < 0 || this.origin.getY() + j < 0
						|| this.origin.getX() + i >= matrix.getRows()
						|| this.origin.getY() + j >= matrix.getCols()) {
					tile = Tile.VOID;
				} else {
					tile = matrix.get(this.origin.getX() + i, this.origin.getY() + j);
				}
				this.context.drawImage(this.tileset.getTile(tile), j * tileSize, i * tileSize, tileSize, tileSize);
				if(new Position(this.origin.getX() + i, this.origin.getY() + j).equals(this.level.getPlayerPos())) {
					this.context.drawImage(this.tileset.getTile(Tile.PLAYER), j * tileSize, i * tileSize, tileSize, tileSize);
				}
			}
		}
	}
	
}
