package javarogue.ui.game.components;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javarogue.config.ConfigGraphics;
import javarogue.ui.game.GameController;
import javarogue.ui.game.GameViewImpl;

/**
 * <h1>GUI</h1>
 * 
 * A GUI component that handles game's overlay visuals.
 * @see GameViewImpl
 *
 */
public class GUI {

	private Image gui;
	private GraphicsContext context;
	
	private GameController controller;
	
	private Map<String, String> overlayText;
	private Map<Integer, String> inventoryText;
	private Font font;
	
	// State booleans
	private boolean isInventoryOpened;
	
	/**
	 * 
	 * @param context Rendering context
	 */
	public GUI(GraphicsContext context, GameController controller) {
		this.loadImage();
		this.initText();
		this.context = context;
		this.controller = controller;
	}

	/**
	 *  renders the camera data on the context.
	 */
	public void draw() {
		double width = this.context.getCanvas().getWidth();
		double height = this.context.getCanvas().getHeight();
		if (this.isInventoryOpened) {
			this.context.setFill(Color.BLACK);
			this.context.fillRect(0, 0, width, height);
			this.context.setFill(Color.WHITE);
			this.context.fillText(this.inventoryText.get(0), width / 20, height / 18);
		} else {
			this.drawHUD(width, height);
		}
	}
	
	/**
	 *  signals the GUI to enter/exit inventory drawing state.
	 */
	public void changeInventoryState() {
		if(this.isInventoryOpened) {
			this.isInventoryOpened = false;
		} else {
			this.isInventoryOpened = true;
		}
	}
	
	private void drawHUD(double width, double height) {
		
		updateText();

		double hOffset = width / 100;
		double vOffset = height - height / 10;

		double spacing = ConfigGraphics.resolutionWidth() / 60;
		
		this.context.drawImage(this.gui, 0, 0, width, height);
		this.context.setFont(this.font);
		this.context.setFill(Color.WHITE);
		// Health Text
		this.context.fillText(this.overlayText.get("health"), hOffset, vOffset);
		this.context.fillText(this.overlayText.get("healthVal"), hOffset + spacing * 8, vOffset);
		// Hunger Text
		this.context.fillText(this.overlayText.get("hunger"), hOffset, vOffset + spacing * 2);
		this.context.fillText(this.overlayText.get("hungerVal"), hOffset + spacing * 8, vOffset + spacing * 2);
		// Level Text
		this.context.fillText(this.overlayText.get("level"), hOffset + spacing * 12, vOffset);
		this.context.fillText(this.overlayText.get("levelVal"), hOffset + spacing * 20, vOffset);
		// Depth Text
		this.context.fillText(this.overlayText.get("depth"), hOffset + spacing * 12, vOffset + spacing * 2);
		this.context.fillText(this.overlayText.get("depthVal"), hOffset + spacing * 20, vOffset + spacing * 2);
	}

	private void loadImage() {
		try {
			this.gui = new Image(this.getClass().getResourceAsStream("/GUI.png"), ConfigGraphics.resolutionWidth(),
					ConfigGraphics.resolutionHeight(), true, true);
		} catch (NullPointerException e) {
			throw new IllegalStateException("GUI.png not found!");
		}
	}
	
	private void initText() {
		try {
			this.font = Font.loadFont(this.getClass().getResource("/font.ttf").toExternalForm(), ConfigGraphics.resolutionWidth() / 60);
		} catch (NullPointerException e) {
			throw new IllegalStateException("font.ttf file not found!");
		}
		this.overlayText = new HashMap<>();
		this.overlayText.put("health", "Health: ");
		this.overlayText.put("healthVal", "X");
		this.overlayText.put("hunger", "Hunger: ");
		this.overlayText.put("hungerVal", "X");
		this.overlayText.put("level", "Level: ");
		this.overlayText.put("levelVal", "X");
		this.overlayText.put("depth", "Depth: ");
		this.overlayText.put("depthVal", "X");
		
		this.inventoryText = new HashMap<>();
		this.inventoryText.put(0, "This is an inventory prototype.");
	}
	
	private void updateText() {
		this.overlayText.put("depthVal", String.valueOf(this.controller.getCurrentDepth()));
	}
	
}
