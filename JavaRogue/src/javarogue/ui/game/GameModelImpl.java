package javarogue.ui.game;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javarogue.generation.LevelFactory;
import javarogue.generation.LevelFactoryImpl;
import javarogue.level.Level;

/**
 * <h1>GameModelImpl</h1>
 * 
 * Implementation of {@link GameModel}.
 *
 */
public class GameModelImpl implements GameModel {

	private GameController controller;
	
	private List<Level> levels;
	private Optional<Level> currentLevel;
	private int currentDepth;
	
	private LevelFactory levelFactory;
	
	public GameModelImpl() {
		this.levelFactory = new LevelFactoryImpl();
		this.levels = new LinkedList<>();
	}
	
	@Override
	public void changeLevel(int level) {
		if(level >= 0 && level <= 3) {
			this.currentDepth = level;
			this.currentLevel = Optional.of(this.levels.get(level));
		}
	}
	
	@Override
	public void generateLevels() {
		for(int i = 0; i < 3; i++) {
			Level level = this.levelFactory.generateSimpleLevel(i);
			level.setController(this.controller);
			this.levels.add(i, level);
		}
		Level bossLevel = this.levelFactory.generateBossLevel();
		bossLevel.setController(this.controller);
		this.levels.add(3, bossLevel);
	}

	@Override
	public Optional<Level> getCurrentLevel() {
		return this.currentLevel;
	}

	@Override
	public int getCurrentDepth() {
		return this.currentDepth;
	}

	@Override
	public void setController(GameController controller) {
		this.controller = controller;
	}
	
}
